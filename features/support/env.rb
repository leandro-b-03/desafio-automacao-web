###################################################################################
# Created by spriteCloud B.V. (R)
# For info and questions: support@spritecloud.com
###################################################################################
require 'rubygems'
require 'selenium-webdriver'
require 'logger'
require 'uri'
require 'time'
require 'watir'
require 'webdriver-user-agent'
require 'lapis_lazuli'
require 'lapis_lazuli/cucumber'

###################################################################################
# Start the logger
$log = Logger.new('log/selenium.log')

###################################################################################
# Read the config file (load config_local.yml if it exists)
begin
  CONFIGS = YAML.load_file("config/config_local.yml")
rescue
  CONFIGS = YAML.load_file("config/config.yml")
end

###################################################################################
# Load the global variables
$T_START = Time.now

###################################################################################
# Launch the browser. Can be firefox, chrome, safari or ie. Defaults to firefox
# Define what browser to use from cucumber yml or from the command line using BROWSER=<..>.

if Gem.win_platform? # If windows
  ENV['HTTP_PROXY'] = ENV['http_proxy'] = nil

  # Firefox
  firefoxdriver_path = 'D:/browsers/geckodriver.exe'
  raise "Cannot found FirefoxDriver on #{firefoxdriver_path}" if !File.file?(firefoxdriver_path)
  Selenium::WebDriver::Firefox.driver_path = firefoxdriver_path

  # Chrome
  chromedriver_path = 'D:/browsers/chromedriver.exe'
  raise "Cannot found ChromeDriver on #{chromedriver_path}" if !File.file?(chromedriver_path)
  Selenium::WebDriver::Chrome.driver_path = chromedriver_path
end

# # Set browser
# browser = case ENV['BROWSER']
# when /chrome/i
#   :chrome
# else
#   :firefox
# end

# puts ENV['URL']

# # Set some initial settings for the brownser based in the type of website
# if [:chrome, :firefox].include? browser
#   if (ENV['MOBILE'] == 'true')
#     driver = Webdriver::UserAgent.driver(:browser => browser, :agent => :iphone, :orientation => :portrait)
#     browser = Watir::Browser.new driver
#   else
#     client = Selenium::WebDriver::Remote::Http::Default.new
#     browser = Watir::Browser.new browser, http_client: client
#     browser.driver.manage.window.maximize
#   end
# else
#   browser =  Watir::Browser.new browser
# end

LapisLazuli::WorldModule::Config.config_file = "config/config.yml"
World(LapisLazuli)

# Returns the error as a string if one is detected
# Here you can also insert custom checks. E.g detect if there is any error box and pass the content
def error_on_page?
  begin
    page_text = browser.html
    CONFIGS['error_strings'].each do |error|
      if page_text.scan(error)[0]
        return page_text.scan(error)[0]
      end
    end
  rescue
    $log.debug "Cannot read html for page #{browser.url}"
  end
  return false
end

LapisLazuli.Start do
  # Print gem information
  print "---- VERSION INFO ----\n"
  print "Lapis Lazuli: #{Gem.loaded_specs['lapis_lazuli'].version}\n"
  print "Selenium webdriver: #{Gem.loaded_specs['selenium-webdriver'].version}\n"
  print "Watir: #{Gem.loaded_specs['watir'].version}\n"
  print "---- VERSION INFO ----\n\n"
  
  browser.driver.manage.window.maximize
end

# Actions that will happen before every scenario
Before do
  # things that should be run before every run
  @browser = browser
  @page = lambda {|b, klass| klass.new b }.curry.(browser)

  @url = ENV["URL"]
end

# This is executed after every scenario.
After do
  # browser.take_screenshot

  #Check if one of the error strings from the config file is detected on the page
  errors_on_page = error_on_page?
  if errors_on_page
    error "'#{errors_on_page}' found on <a href='#{browser.url}' target='_blank'>page</a>"
  end

  #Wait the required time
  sleep CONFIGS['step_pause_time'] rescue sleep 0
end

at_exit do
  browser.cookies.clear
  browser.close
end