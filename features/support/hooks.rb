require 'json'
require 'sequel'

Before do |scenario|
  # Set a variable wiht the feature and scenario name
  @feature_name = scenario.feature.name
  @scenario_name = scenario.name

  # Set an error variable array
  $tagerros ||= []
  scenario_tags = scenario.source_tag_names

  # Set a variable with scenario tags
  @feature_tag = scenario_tags.first

  # Set screenshots path
  @screenshots_path = "report/screenshots/#{Time.now.strftime("%d%m%Y%H%M")}"
  FileUtils.mkpath @screenshots_path
  
  # Time that scenario started
  puts "Scenario started in: #{DateTime.now}"
end

After do |scenario|
  # Clean browser session
  @browser.cookies.clear rescue warn 'Without session to clean'
  @step = 0

  # Time that scenario ended
  puts "Scenario ended in: #{DateTime.now}"
end

AfterStep do |scenario|
  @step ||= 0
  encoded_img = "#{@screenshots_path}/#{Time.now.strftime("%d%m%Y%H%M")}-Passo #{@step+1}.png"
  @browser.screenshot.save(encoded_img)
  embed encoded_img , 'image/png'
  @step += 1
end

def send_email(report)
  # First, instantiate the Mailgun Client with your API key
  @mg_client = Mailgun::Client.new 'key-46dbf5c630e27abe3a85598a182598f0'

  template = File.read(Dir.pwd + '/features/support/mail/report.html')

  mb_obj = Mailgun::MessageBuilder.new()

  # Define the from address.
  mb_obj.from("leandro.bezerra@centauro.com.br", {"first"=>"FredWeb", "last" => "Automation"});

  emails = Emails.where('env = ?', ENV['URL']).where('active = ?', 1).all

  emails.each { |recipient|
    # Define a to recipient.
    mb_obj.add_recipient(:to, recipient.email)
  }

  # Define the subject.
  mb_obj.subject("A new test run! - FredWeb")

  # Define the body.
  mb_obj.body_html(template.gsub('*|env|*', ENV['URL']).gsub("*|CURRENT_YEAR|*", Date.today.year.to_s))

  # Attach a file and rename it.
  mb_obj.add_attachment(File.join(Dir.pwd, report), report)

  # Send your message through the client
  @mg_client.send_message 'send-mail.someideias.com', mb_obj
end