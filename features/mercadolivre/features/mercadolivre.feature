# language: pt

@busca
Funcionalidade: Busca

  Cenário: Buscar no site
    Dado que eu estou na home
    Quando eu pesquiso "Macbook Pro"
      E encontro uma pagina de resultados com o termo "Macbook Pro"
    Então imprimo os 5 primeiros titulos e valores

  Cenário: Buscar no site
    Dado que eu estou na home
    Quando eu pesquiso "Macbook Pro"
      E encontro uma pagina de resultados com o termo "Macbook Pro"
      E prossigo para a página "2"
      E entro no penútimo anuncio da página
    Então eu tiro print do conteúdo