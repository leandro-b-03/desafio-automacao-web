################################################################################
# Copyright 2017.
# Author: "Leandro Bezerra" <leandro.b.03@gmail.com>

# home_screen.rb is used to interact with elements on the page.
################################################################################

################################################################################
# Class to get all elements of the screen
################################################################################
class ProductScreen
  def initialize(browser)
    # Get all the browser elements
    @browser = browser

    # Get element by term. Ex: class, id, text and xpath
    @search_title = @browser.h1(class: 'item-title__primary')
    @search_result = @browser.ol(id: 'searchResults')
    @search_pagination = @browser.ul(class: ['pagination',  'grid', 'u-clearfix'])
	end

	def screenshot_product()
    sleep 4

    @browser.screenshot.save "screenshots/screenshot-#{Time.now.strftime("%d%m%Y%H%M")}.png"
	end
end
