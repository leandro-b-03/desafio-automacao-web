################################################################################
# Copyright 2017.
# Author: "Leandro Bezerra" <leandro.b.03@gmail.com>

# home_screen.rb is used to interact with elements on the page.
################################################################################

################################################################################
# Class to get all elements of the screen
################################################################################
class SearchScreen
  def initialize(browser)
    # Get all the browser elements
    @browser = browser

    # Get element by term. Ex: class, id, text and xpath
    @search_title = @browser.h1(class: 'breadcrumb__title')
    @search_result = @browser.ol(id: 'searchResults')
    @search_pagination = @browser.ul(class: ['pagination',  'grid', 'u-clearfix'])
	end

	def validate_search(term)
  	if (@search_title.text.downcase != term.downcase)
  		raise 'The term and the title doens\'t match'
  	end

  	if (@search_result == nil)
  		raise 'There\'s no results'
  	end
	end

  def print_result(times)
    # puts @search_result.elements(:class, ['results-item', 'article', 'grid']).count

    item_list = @search_result.elements(:class, ['results-item', 'article', 'grid'])

    item_list.each_with_index do |item, index|
      if (index < times.to_i)
        puts printf('%-5s %5s' % ['', "Título #{index + 1}: " + item.element(:class, ['item__title', 'list-view-item-title']).text])
        puts printf('%-5s %5s' % ['', "Preço #{index + 1}: " + item.element(:class, 'item__price').text])
      else
        break
      end
    end
  end

  def go_to_page(number)
    page_list = @search_pagination.elements(:class, 'pagination__page')

    page_list.each do |page_number|
      if (page_number.text == number)
        page_number.click
        
        break
      end
    end
  end

  def go_to_product()
    item_list = @search_result.elements(:class, ['results-item', 'article', 'grid'])

    item_list[item_list.size - 2].click
  end
end
