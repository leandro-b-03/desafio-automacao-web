################################################################################
# Copyright 2017.
# Author: "Leandro Bezerra" <leandro.b.03@gmail.com>

# home_screen.rb is used to interact with elements on the page.
################################################################################

################################################################################
# Class to get all elements of the screen
################################################################################
class HomeScreen
  def initialize(browser)
    # Get all the browser elements
    @browser = browser

    # Get element by term. Ex: class, id, text and xpath
    @search_field = @browser.text_field(class: 'nav-search-input')
    @search_button = @browser.button(class: 'nav-search-btn')
	end

	def search(term)
		@search_field.set(term)
		@search_button.click
	end
end
