# Tests for Desafio-Automacao-Web

# Setup

## General

- Make sure you have ruby 1.9 or later installed.
- Make sure you have firefox and/or chrome installed
- Install the bundler gem:

    ```
    $ gem install bundler
    ```

- Install all of the required gems defined in the gemfile:

    ```
    $ bundle install
    ```

- Run cucumber through bundler:

    ```
    $ bundle cucumber URL=www.mercadolivre.com.br -p ff -p desktop1080
    ```

## Recomended Editors

- Editor to read and edit the project files:

    Sublime Text 3 and VSCode